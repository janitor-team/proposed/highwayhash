highwayhash (0~git20200803.9490b14-3) unstable; urgency=medium

  * Give up tracking C++ symbols. (Closes: #984175)
  * Fix FTCBFS due to multiple reasons; Patch rebased. (Closes: #970832)
  * Bump Standards-Version to 4.6.0 (no change).
  * Strip unnecessary shlibs:Depends from -dev package.
  * Remove unused lintian overrides.

 -- Mo Zhou <lumin@debian.org>  Wed, 24 Nov 2021 16:28:14 -0500

highwayhash (0~git20200803.9490b14-2) unstable; urgency=medium

  [ Yangfl ]
  * Fix FTBFS on most archs due to problematic Makefile. (Closes: #986273)

  [ Mo Zhou ]
  * Rename libhighwayhash0.symbols -> lib{...}.symbols.amd64.

 -- Mo Zhou <lumin@debian.org>  Mon, 05 Apr 2021 10:52:10 +0800

highwayhash (0~git20200803.9490b14-1) unstable; urgency=medium

  * New upstream version 0~git20200803.9490b14
  * Fixup architecture detection.
  * Deprecate d/compat and bump debhelper compat level to 13.
  * Update my mail address in control and copyright.
  * Specify Rules-Requires-Root: no
  * Fixup installation part in d/rules.
  * Refresh symbols control file for amd64, arm64, ppc64el.

 -- Mo Zhou <lumin@debian.org>  Tue, 22 Sep 2020 10:50:25 +0800

highwayhash (0~git20191002.0aaf66b-1) unstable; urgency=medium

  * New upstream version 0~git20191002.0aaf66b
  * Collect symbols update from buildd for ppc64el.

 -- Mo Zhou <cdluminate@gmail.com>  Fri, 04 Oct 2019 07:48:51 +0000

highwayhash (0~git20190222.276dd7b-1) unstable; urgency=medium

  * New upstream version 0~git20190222.276dd7b
  * Refresh symbols.

 -- Mo Zhou <cdluminate@gmail.com>  Sun, 24 Feb 2019 02:25:23 +0000

highwayhash (0~git20181002.c5ee50b-3) unstable; urgency=medium

  * Team upload
  * Really build on arch:any

 -- Aron Xu <aron@debian.org>  Mon, 26 Nov 2018 23:06:33 +0800

highwayhash (0~git20181002.c5ee50b-2) unstable; urgency=medium

  * Team upload
  * Build on arch:any

 -- Aron Xu <aron@debian.org>  Mon, 26 Nov 2018 22:37:21 +0800

highwayhash (0~git20181002.c5ee50b-1) unstable; urgency=medium

  * Change pretty format string in watch file.
  * New upstream version 0~git20181002.c5ee50b

 -- Mo Zhou <cdluminate@gmail.com>  Sun, 25 Nov 2018 02:02:55 +0000

highwayhash (0~20180209-g14dedec-6) unstable; urgency=medium

  * Collect symbols update from buildd. (Closes: #897767)
    + Update symbols for arm64, ppc64el, x32 .
  * Bump Standards-Version to 4.2.0 (no change).

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 23 Aug 2018 05:47:28 +0000

highwayhash (0~20180209-g14dedec-5) unstable; urgency=medium

  * Refresh symbols to avoid FTBFS with GCC-8 (Closes: #897767)
  * Bump Standards-Version to 4.1.5 (no change).

 -- Mo Zhou <cdluminate@gmail.com>  Sun, 22 Jul 2018 02:52:36 +0000

highwayhash (0~20180209-g14dedec-4) unstable; urgency=medium

  * Depending on build-essential to really fix autopkgtest failure.
  * Add watch file to monitor upstream git HEAD.
  * Don't override debian-watch-file-is-missing, already fixed.
  * Bump Standards-Version to 4.1.4 (no change).

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 28 Jun 2018 05:16:34 +0000

highwayhash (0~20180209-g14dedec-3) unstable; urgency=medium

  * Set Ccience team as the maintainer
    + Put myself to Uploaders.
  * Move the packaging repo to salsa science team's namespace
    + Update Vcs-* links in control.
  * Add gcc to autopkgtest depends to fix CI test failure.
  * Collect symbol updates for x32 from buildd.

 -- Mo Zhou <cdluminate@gmail.com>  Tue, 03 Apr 2018 16:07:22 +0000

highwayhash (0~20180209-g14dedec-2) unstable; urgency=medium

  * Remove ppc64 from Architecture list as suggested by upstream.
    + https://github.com/google/highwayhash/issues/66
    - Remove ppc64 symbols.
  * Collect symbols update from buildd for arm64 and ppc64el.
  * Upload to unstable.

 -- Mo Zhou <cdluminate@gmail.com>  Tue, 27 Mar 2018 09:04:28 +0000

highwayhash (0~20180209-g14dedec-1) experimental; urgency=medium

  * New upstream snapshot 0~20180209-g14dedec
  * Remove all patches since are included in this snapshot.
  * Refresh symbols for amd64.
  * Remove debian/highwayhash.3 which was already merged in upstream source.

 -- Mo Zhou <cdluminate@gmail.com>  Sun, 25 Mar 2018 08:39:28 +0000

highwayhash (0~20180103-geeea446-2) experimental; urgency=medium

  * Cherry-pick upstream fix for ppc64.
    + up_add_file_update_makefile_for_ppc64_vsx.patch
  * Collect symbols update for arm64 and x32 from buildd.
    Refresh symbols for amd64. (Closes: #877130)
  * Don't build anymore on architectures that are not supported by upstream.
  * Check DEB_BUILD_PROFILES on the overriden dh_auto_test target.
  * Fix package-uses-deprecated-source-override-location.
  * Override symbols-file-contains-debian-revision which is a false-positive.

 -- Mo Zhou <cdluminate@gmail.com>  Wed, 07 Feb 2018 04:25:14 +0000

highwayhash (0~20180103-geeea446-1) experimental; urgency=medium

  * Import upstream snapshot eeea4463df1639c7ce271a1d0fdfa8ae5e81a49f .
  * Remove all patches because they were merged by upstream.
    - makefile-disable-siptreehash-and-make-solib.patch
    - makefile-target-install.patch
  * compat: Bump debhelper compat to 11
  * control: Add new B-D "rename" and Vcs-* fields
  * control: Add missing depends to dev package (Closes: #877207)
  * control: Bump Standards-Version to 4.1.3 (no change)
  * copyright: fix insecure-copyright-format-uri
  * rules: fix build and installation.
  * Symbols: refresh. (Closes: #877130)
  * Update my name.

 -- Mo Zhou <cdluminate@gmail.com>  Thu, 18 Jan 2018 10:07:06 +0000

highwayhash (0~20170419-g1f4a24f-2) experimental; urgency=medium

  * Collect symbols patch from buildd for arm64, ppc64, ppc64el and x32.
  * Make SYNOPSIS section of highwayhash.3 less ambiguous.
  * Rules: trivial update.

 -- Zhou Mo <cdluminate@gmail.com>  Thu, 27 Apr 2017 02:15:24 +0000

highwayhash (0~20170419-g1f4a24f-1) experimental; urgency=medium

  * Initial release. (Closes: #848885)

 -- Zhou Mo <cdluminate@gmail.com>  Tue, 25 Apr 2017 04:13:09 +0000
